<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/7/19
 * Time: 3:57 PM
 */

namespace App\Traits;


use Illuminate\Http\Response;

trait ApiResponser
{
    public function successResponse($data, $code = Response::HTTP_OK)
    {
        return response()->json(['data' => $data], $code);
    }

    public function errorResponse($message, $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }
}
