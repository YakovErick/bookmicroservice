<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/7/19
 * Time: 4:07 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title',
        'description',
        'price',
        'author_id',
    ];
}
